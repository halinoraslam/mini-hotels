// variables
var pageWrap = $('.wrapper');
var contentWrap = $('.content');
var overlay = $('<div class="overlay"></div>');
var minHeight = 0;
// page height resize
var supportsOrientationChange = "onorientationchange" in window;
var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
window.addEventListener(orientationEvent, myResize, false);
var countInput;
$(document).ready(function(){
// start
	// page height resize
	myResize();
	// menu trigger
	if ($('.btnMenu').length>0) {
		$('.btnMenu').on('click', function(){
			$(this).toggleClass('in');
			if ($(this).hasClass('in')) {
				pageWrap.addClass('menu_op');
				$('.navigation').fadeIn(500);
				$('.navigation li').each(function(index){
					var t1 = $(this);
					setTimeout(function(){
						$(t1).removeClass('unshown');
						$(t1).addClass('shown');
					},index*200);
				});
				pageWrap.append(overlay);
			}
			else {
				$('.overlay').remove();
				pageWrap.removeClass('menu_op');
				$('.navigation li').each(function(index){
					var t1 = $(this);
					setTimeout(function(){
						$(t1).removeClass('shown');
						$(t1).addClass('unshown');
					},index*100);
				});
				setTimeout(function(){
					$('.navigation').fadeOut(500);
				},500);
			}
		});
	}
	// exit trigger
	if ($('.btnExit').length>0) {
		$('.btnExit').on('click', function(){
			confirm("Вы уверены, что хотите выйти из аккаунта?");
			return false;
		});
	}
	// price tags manipulation
	if ($('.price-tags').length>0) {
		$('.tag').on('click', function(){
			var priceValue = $(this).attr('data-price');
			$('#price').val(priceValue);
			return false;
		});
	}
	// user adding & user remove
	if ($('.link-add').length>0) {
		$('.link-add').on('click', function() {
			var userAdding = $('#add-form .inner').first().html();
			$('#add-form').append('<div class="inner">'+userAdding+'</div>');
			$('#add-form .inner').last().append('<a href="" target="" class="field-wrap backing remove-user">'+
												'<i class="icon-remove-user"></i><div class="field">'+
												'<span class="description">Удалить гостя</span></div></a>');
			$('.remove-user', $('#add-form .inner').last()).on('click', function(){
				$(this).parent().remove();
				return false;
			});
			return false;
		});
	}
	// tab carousel
	if ($('.tab-controls').length>0) {
		$('.tab-controls a').on('click', function(){
			$('.tab-controls a').removeClass('active');
			$(this).addClass('active');
			var dataTrigger = $(this).attr('data-id');
			$('.tab-content').removeClass('visible')
			$('#'+dataTrigger).addClass('visible');
			return false;
		});
	}
	// item count
	if ($('.counter').length>0) {
		counterCalculation();
	}
	// field generator
	if ($('.main-roller').length>0) {
		$('.counter-trigger').on('change', function(){
			optionFunction($(this));
		});
	}
// end
});

function myResize() {
	minHeight = screen.height;
	$('html').css('min-height',minHeight+'px');
}

function counterCalculation() {
	$('.minus, .plus').unbind('click');
	$('.minus').on('click', function(){
		var countTemp = $(this).parent().find('.item-count');
		if(countTemp.val()>0)
		{
			countTemp.val((parseInt(countTemp.val()))-1);
		}
	});
	$('.plus').on('click', function(){
		var countTemp = $(this).parent().find('.item-count');
		countTemp.val((parseInt(countTemp.val()))+1);
	});
}

function optionFunction(t1) {
	$('.counter-trigger').unbind('change');
	var selectedOptionValue=$("option:selected",t1).attr('data-id');
		console.log(selectedOptionValue);
		$(t1).parent().parent().parent().find('.counter').addClass('visible');
		if ($('.counter').hasClass('visible')) {
			$('.plus').one('click', function(){
				if($('select option',$('.roller').last()).length>2)
				{
					var createdBlock = $('.roller').last().html();
					$('.added-rollers').append('<div class="roller">'+createdBlock+'</div>');
					$('.counter-trigger').on('change', function(){

						optionFunction($(this));
					});
					$('select option',$('.added-rollers .roller').last()).each(function(){
						if($(this).attr('data-id')==selectedOptionValue)
						{
							$(this).remove();
						}
					});
					counterCalculation();
				}
			});
	}
}