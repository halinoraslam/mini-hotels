'use strict';

var gulp = require('gulp');
var jade = require('gulp-jade');
var uglify = require('gulp-uglify');
var csso = require('gulp-csso');
var csscomb = require('gulp-csscomb');
var csslint = require('gulp-csslint');
var uncss = require('gulp-uncss');
var compass = require('gulp-compass');
var connect = require('gulp-connect');
var imagemin = require('gulp-imagemin');
var minifyCSS = require('gulp-minify-css');
var concatCSS = require('gulp-concat-css');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var spritesmith = require('gulp.spritesmith');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('default', ['jade', 'compass', 'css', 'uglify', 'connect', 'watch'])

// tasks
gulp.task('compass', function() {
	gulp.src(['./sass/**/*.sass','./sass/*.scss'])
		.pipe(compass({
			css: 'app/css',
			sass: 'app/sass',
			image: 'app/images'
		}))
		.pipe(connect.reload())
		.pipe(gulp.dest('app/assets/'));
});

gulp.task('uglify', function() {
	return gulp.src('app/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

// gulp.task('sprite', function() {
// 	var spriteData = 
// 	gulp.src('app/images/icon/*.png')
// 		.pipe(spritesmith({
// 			imgPath: '../images/sprite.png',
// 			imgName: 'sprite.png',
// 			retinaImgName: '../images/sprite-2x.png',
// 			retinaSrcFilter: 'app/images/icon/*-2x.png',
// 			cssName: 'sprite.css',
// 			cssFormat: 'sass',
// 			padding: 10
// 		}));

// 		spriteData.img.pipe(gulp.dest('app/temp/images/'));
// 		spriteData.css.pipe(gulp.dest('app/temp/styles/'));
// });


gulp.task('css', function(){
	return gulp.src('app/css/**/*.css')
		.pipe(concatCSS('main.min.css'))
		.pipe(autoprefixer({
			browsers: ['last 50 versions'],
			cascade: false
		}))
		.pipe(csscomb())
		.pipe(csslint())
		.pipe(minifyCSS())
		.pipe(connect.reload())
		.pipe(gulp.dest('build/css'));
});

gulp.task('images', function(){
	return gulp.src('app/images/*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
		}))
		.pipe(connect.reload())
		.pipe(gulp.dest('build/images'));
});

gulp.task('jade', function(){
	return gulp.src('app/templates/*.jade')
		.pipe(jade({
			pretty: true
		}))
		.pipe(connect.reload())
		.pipe(gulp.dest('build/'));
});

gulp.task('watch', function (){
	gulp.watch('app/sass/*.sass', ['compass']);
	gulp.watch('app/css/*.css', ['css']);
	gulp.watch('app/templates/*.jade', ['jade']);
	gulp.watch('app/js/*.js', ['uglify']);
	gulp.watch('app/images/*', ['images']);
});

gulp.task('connect', function() {
	connect.server({
		port: 1337,
		root: 'build',
		livereload: true
	});
});